/*
Buts en bonus, par ordre de difficulté:

Faires des ennemis qui viennent se coller au héros lorsque le héros arrive à une certaine distance.

Introduire des sons!

Générer du bruit de perlin pour générer les tiles de foret, d'herbe, d'eau etc.

Implémenter ou réutiliser un algorithme de pathfinding (djikstra ou A*) sur une grille.

Faire un shader de flou qui diminue lorsque le jeu commence.

*/
