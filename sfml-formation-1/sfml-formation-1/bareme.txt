sur 100 points
30 propreté du code
    10 nom des variables, choix des noms de variable et sémantique des noms
    5 pas de code spaghetti
    5 pas de valeur en dur
    5 pas trop de code dans le main
    5: 0 warnings
20 utilisation des bonnes pratiques
    10 code correct, simple, lisible et compréhensible, appréciation générale
    5 segmentation du code et utilisation de la programmation objet
    5 risques de bug futur (exemple: .find() au lieu de count(), trop de pointeurs inutiles)
    qwerty
    

50 répond au CDC
    10 chargement d'un niveau
        bonus pour passer derrière les arbres/tiles
    10 déplacement du personnage grace au clavier
    5 animation du héros
    5 épée tournante dans la bonne direction
    5 déplacement des monstres sur un chemin avec pauses
    5 objets au sol
    5 fonctionalités supplémentaires
        les ennemis suivent le héros
        inventaire
        tileset customisé, s'il est bien fait et propre

