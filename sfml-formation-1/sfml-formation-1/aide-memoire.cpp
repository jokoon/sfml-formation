#include <map>
using namespace std;
using namespace sf;
typedef Vector2f vec2;
typedef Vector2i vec2i;

struct object{
    void function();
};
void object :: function(){


}

object obj;

obj.function();

// object :: function et obj.function sont la meme fonction, mais :: réfère à la fonction de la classe, pas de l'instance.


// Iterer un vector<string>
vector<string> stories= {
    "Il etait une fois dans Lyon",
    "Il etait une fois dans Toulouse",
    "Il etait une fois dans Brest",
}



// ce code imprime "Lyon", "Toul", "Bres"
for(auto & str : stories){
    // imprimer un substring
    cout << str.substr(24, 4) << endl;
}

// utiliser une map, ou tableau associatif/dictionnaire
map<string,int> m = {
    {"une cle", 342},
    {"chose", 232}
};
// vérifier si une clé est présente:
if (m.count("une cle")){
    cout << "j'ai trouvé la clé!" << endl;
}
// attention: accéder m["fds"] crée un objet vide si la clé n'existe pas !
cout << m["fds"] << endl;
// imprime "0"
// la map aura maintenant une entrée "fds" à 0 (0 étant la valeur par défaut de int pour map, mais pas pour C++):
// {{"une cle", 342}, {"chose", 232}, {"fds", 0}};

// Note: une map<> est toujours triée! Utiliser unordered_map<> sinon

// itérer une map:
map<string, int> m;
for(auto & p : m){ // p est de type pair<string,int>
    p.first; // string
    p.second; // int
}

// fonction d'interpolation:
// Une fonction d'interpolation retourne une valeur entre A et B lorsqu'on lui donne une valeur entre 0 et 1. On peut utiliser une telle fonction pour différents types
// en langage shader, cette fonction s'appelle souvent lerp()
// ATTENTION: il faut impérativement que 0 <= coeff <= 1. utiliser clamp() sinon.
// pour un float
float interp(float start, float end, float coef){
    return coef*(end - start) + start;
}
// pour un vec2
vec2 interp2d(vec2 start, vec2 end, float coef){
    return coef*(end - start) + start;
}
// cette fonction interpole avec 2 coefficients différents sur x et y
vec2 interp2d(vec2 start, vec2 end, vec2 coeff){
    return start + scal(coeff, end - start);
}

// clamp retourne f s'il est compris entre 0 et 1
// sinon il retourne 0 si f < 0, et 1 si f > 1
float clamp(float a, float b, float f){
    return min(1, max(0, f));
}

// la fonction circlepoint retourne un point placé sur un cercle, selon un centre, un rayon et un angle entre 0 et 1, équivalent à 0 -> 360 degrés ou 0 -> 2*pi radians.
vec2 circlepoint(vec2 center, float radius, float pi){
    return
    center
    + vec2(
        radius*cos(float(PI)*((pi) * 2)),
        radius*sin(float(PI)*(1.0f-pi) * 2));
    }
// la fonction smoothstep retourne une fonction à peu près sigmoide, pour "lisser" le déplacement d'un objet dans le temps
// son équation: f(x) = 3x^2 - 2x^3
// il existe aussi f(x) = 6x^5 - 15x^4 + 10x^3

// https://en.wikipedia.org/wiki/Smoothstep
// https://en.wikipedia.org/wiki/Sigmoid_function
float smoothstep(float coef) { return 3.f*coef*coef - 2.f*coef*coef*coef; }

// la fonction "scal" multiplie les composantes de A et de B

vec2 scal(vec2 a, vec2 b)
{ return vec2(a.x*b.x, a.y*b.y); }

// version template:
template<typename T>
Vector2<T> scal(Vector2<T> a, Vector2<T> b)
{ return Vector2<T>(a.x*b.x, a.y*b.y); }

// Bonus 1: les set<>
// Un set est un tableau ou les valeurs sont uniques.
set<int> ensemble = { 1, 1, 2, 3 };
// ici le set n'a que les valeurs 1 2 3
ensemble.insert(2); // aucun effet
ensemble.remove(3);
ensemble[1]; // erreur de compilation: il n'est pas possible de récupérer les objets par index. Il faut utiliser:
ensemble.count(6); // retourne 0
ensemble.count(1); //  retourne1
// il existe
// Bonus 2: les multi_map

multi_map<string, int> super_dictionnaire = {
    {"1", 453},
    {"1", 4524},
    {"1", 41121},
    {"2", 41121},
};
super_dictionnaire.insert({"2", 55234});
super_dictionnaire.insert({"2", 1111});
super_dictionnaire.count("2"); // retourne 3

auto query = super_dictionnaire.equals_range("1");
for (auto a = query.first; a != query.second; ++a){
    cout << "1" << a << endl;

}
// il existe aussi le multi_set